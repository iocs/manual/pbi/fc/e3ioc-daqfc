require adsis8300fc
require busy
require admisc,2.1.2
require calc
require essioc

errlogInit(20000)
callbackSetQueueSize(15000)

epicsEnvSet("CONTROL_GROUP", "PBI-FC01")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-5")
epicsEnvSet("EVR_NAME", "$(CONTROL_GROUP):Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "LEBT-010:PBI-FC-001:")
epicsEnvSet("SYSTEM1_NAME", "CH1")
epicsEnvSet("SYSTEM1_CHANNEL", "0")
epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-FC-001:")
epicsEnvSet("SYSTEM2_NAME", "CH2")
epicsEnvSet("SYSTEM2_CHANNEL", "1")
epicsEnvSet("SYSTEM3_PREFIX", "DTL-020:PBI-FC-001:")
epicsEnvSet("SYSTEM3_NAME", "CH3")
epicsEnvSet("SYSTEM3_CHANNEL", "2")
epicsEnvSet("SYSTEM4_PREFIX", "DTL-040:PBI-FC-001:")
epicsEnvSet("SYSTEM4_NAME", "CH4")
epicsEnvSet("SYSTEM4_CHANNEL", "3")

#iocshLoad("$(adsis8300fc_DIR)/daqfc-main.iocsh")
iocshLoad("$(E3_CMD_TOP)/iocsh/daqfc-main.iocsh")

# data channels
# each channel corresponds to one FC system
iocshLoad("$(adsis8300fc_DIR)fc_channel.iocsh", "ADDR=$(SYSTEM1_CHANNEL),NAME=$(SYSTEM1_NAME),PREFIX=$(SYSTEM1_PREFIX),CG_PREFIX=$(PREFIX)")
iocshLoad("$(adsis8300fc_DIR)fc_channel.iocsh", "ADDR=$(SYSTEM2_CHANNEL),NAME=$(SYSTEM2_NAME),PREFIX=$(SYSTEM2_PREFIX),CG_PREFIX=$(PREFIX)")
iocshLoad("$(adsis8300fc_DIR)fc_channel.iocsh", "ADDR=$(SYSTEM3_CHANNEL),NAME=$(SYSTEM3_NAME),PREFIX=$(SYSTEM3_PREFIX),CG_PREFIX=$(PREFIX)")
iocshLoad("$(adsis8300fc_DIR)fc_channel.iocsh", "ADDR=$(SYSTEM4_CHANNEL),NAME=$(SYSTEM4_NAME),PREFIX=$(SYSTEM4_PREFIX),CG_PREFIX=$(PREFIX)")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

# Compressed record of the waveforms to save in archiver
# NSAM : Compressed Array length
# N : Decimation factor (N to 1 Avergae)
dbLoadRecords("$(E3_CMD_TOP)/db/decimatedwaveforms.db", "P=$(SYSTEM1_PREFIX)TRC2-ArrayData, N=10, NSAM=12000")
dbLoadRecords("$(E3_CMD_TOP)/db/decimatedwaveforms.db", "P=$(SYSTEM2_PREFIX)TRC2-ArrayData, N=10, NSAM=12000")
dbLoadRecords("$(E3_CMD_TOP)/db/decimatedwaveforms.db", "P=$(SYSTEM3_PREFIX)TRC2-ArrayData, N=10, NSAM=12000")
dbLoadRecords("$(E3_CMD_TOP)/db/decimatedwaveforms.db", "P=$(SYSTEM4_PREFIX)TRC2-ArrayData, N=10, NSAM=12000")

# Process STAT3-MeanValueR with STAT3-MeanValue_RBV
dbLoadRecords($(E3_CMD_TOP)/db/ValueR_RBV_forwardlink.db, "P=$(SYSTEM1_PREFIX)")
dbLoadRecords($(E3_CMD_TOP)/db/ValueR_RBV_forwardlink.db, "P=$(SYSTEM2_PREFIX)")
dbLoadRecords($(E3_CMD_TOP)/db/ValueR_RBV_forwardlink.db, "P=$(SYSTEM3_PREFIX)")
dbLoadRecords($(E3_CMD_TOP)/db/ValueR_RBV_forwardlink.db, "P=$(SYSTEM4_PREFIX)")

# ROI records of the waveforms to show on POS
iocshLoad("$(E3_CMD_TOP)/iocsh/ROIwaveform.iocsh","PREFIX=$(SYSTEM1_PREFIX),NAME=$(SYSTEM1_NAME),CG_PREFIX=$(PREFIX)")
iocshLoad("$(E3_CMD_TOP)/iocsh/ROIwaveform.iocsh","PREFIX=$(SYSTEM2_PREFIX),NAME=$(SYSTEM2_NAME),CG_PREFIX=$(PREFIX)")
iocshLoad("$(E3_CMD_TOP)/iocsh/ROIwaveform.iocsh","PREFIX=$(SYSTEM3_PREFIX),NAME=$(SYSTEM3_NAME),CG_PREFIX=$(PREFIX)")
iocshLoad("$(E3_CMD_TOP)/iocsh/ROIwaveform.iocsh","PREFIX=$(SYSTEM4_PREFIX),NAME=$(SYSTEM4_NAME),CG_PREFIX=$(PREFIX)")

# apply Access Security Group values
set_pass0_restoreFile("$(E3_CMD_TOP)/autosave/access_security.sav")

# apply default PV values
set_pass1_restoreFile("$(E3_CMD_TOP)/autosave/default_settings_cg.sav", "P=$(PREFIX),R=")
set_pass1_restoreFile("$(E3_CMD_TOP)/autosave/default_settings_system1.sav", "P=$(SYSTEM1_PREFIX),R=,SCALE=0.0110771,OFFSET=-32567.11,FLATTOP_START=62000,FLATTOP_SIZE=2500")
set_pass1_restoreFile("$(E3_CMD_TOP)/autosave/default_settings_system2.sav", "P=$(SYSTEM2_PREFIX),R=,SCALE=0.010965 ,OFFSET=-32645.09,FLATTOP_START=64050,FLATTOP_SIZE=20")
set_pass1_restoreFile("$(E3_CMD_TOP)/autosave/default_settings_system3.sav", "P=$(SYSTEM3_PREFIX),R=,SCALE=1        ,OFFSET=0        ,FLATTOP_START=0    ,FLATTOP_SIZE=1")
set_pass1_restoreFile("$(E3_CMD_TOP)/autosave/default_settings_system4.sav", "P=$(SYSTEM4_PREFIX),R=,SCALE=0.01104  ,OFFSET=-32701.07,FLATTOP_START=64040,FLATTOP_SIZE=40")

# See https://gitlab.esss.lu.se/e3/wrappers/core/e3-auth/blob/master/template/fc_security.acf
epicsEnvSet("ASG_FILENAME", "fc_security.acf")
iocshLoad("$(essioc_DIR)/common_config.iocsh")
