# e3ioc-daqfc

e3 ioc - Data Acquisition Faraday Cups

## Cloning

Clone this IOC with `git clone https://gitlab.esss.lu.se/iocs/pbi/fc/e3ioc-daqfc.git`.

## Running the IOC

This IOC doesn't need to be compiled nor installed in a specific directory. Just make sure that the versions of the modules required in `st.cmd` are installed for the EPICS base and `require` version that you are using.

If your E3 environment is activated, just go to the top directory of this IOC and run `iocsh.bash st.cmd`.

## Needed libs
* hdf5
* libtiff
* blosc
* netcdf

```mermaid
graph TB
subgraph "Asyn ports processing order"
Ctrl-AMC-110 --> PROC1 & TRC1 & CB1 & HDF1
PROC1 --> ROI2 --> PROC2 --> TRC2 --> ROI4 --> TRC4
ROI2 --> STAT2 --> STAT2-TS
PROC1 --> ROI3 --> PROC3 --> TRC3
ROI3 --> STAT3 --> STAT3-TS
end
```
